<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\CommandType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(ProductRepository $repository, Request $request, SessionInterface $session)
    {
        
        $form = $this->createForm(CommandType::class);

        $total=0;
        $panier = $session->get('panier');
        $products = [];
        if($panier){
            foreach($panier as $id => $quantity) {
                $p =$repository->find($id);
                $products[$id] = [
                    "product"=>$p,
                    "quantite"=>$quantity,
                ];
                $total+=$p->getPrice()*$quantity;
            }
        }

        $form->handleRequest($request);
        $command=$form->getData();
        $manager = $this->getDoctrine()->getManager();
        if ($form->isSubmitted()) {
      
            foreach($panier as $id => $quantity) {
                $p =$repository->find($id);
                $command->addProduct($p);
            }
        
            $command->setCreatedAt(new \DateTime ('now'));
            $manager->persist($command);
            $manager->flush();
  
        }

        
        return $this->render('panier/panier.html.twig',[
            'controller_name' => 'PanierController',
            'products' => $products,
            'total' => $total,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/panier/delete/{id}", name="panier.delete")
     */
    public function delete(ProductRepository $repository,$id, SessionInterface $session, Request $request)
    {
        $csrfToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-item', $csrfToken)) {
            $product = $repository->find($id);
            if (!$product) {
                throw $this->createNotFoundException('The product does not exist');
            }

            $panier = $session->get('panier', []);
            if (isset($panier)) {
                if (isset($panier[$id])) {
                    unset($panier[$id]);
                    $session->set('panier', $panier);
                    $this->addFlash('success', "Le produit {$product->getName()} a bien été retiré du panier !");
                } else {
                    $this->addFlash('danger', "Le produit n'est pas présent dans le panier !");
                }
            }else{
                $this->addFlash('danger', "Le panier est vide !");
            }
        }else{
            throw new InvalidCsrfTokenException;
        }

        return $this->redirectToRoute('panier');
    }


    
}