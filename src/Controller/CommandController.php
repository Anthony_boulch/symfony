<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Product;
use App\Repository\CommandRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandController extends AbstractController
{
    private $repository;

    public function __construct(CommandRepository $repository){
        $this->repository = $repository;
    }


    /**
     * @Route("/command", name="command")
     */
    public function index()
    {
        $commands = $this->repository->findAll();
        //dd($articles);
        return $this->render('command/Command.html.twig', [
            'controller_name' => 'CommandController',
            'commands' => $commands
        ]);
        
    }

     /**
   * @Route("/command/{id}", name="command.show")
    */
    public function show(string $id)
    {
        $CommandRepository = $this->getDoctrine()->getRepository(Command::class);
        $command = $CommandRepository->find($id);
        $total=0;
        $products = $command->getProducts();
        if($command){
            foreach($products as $product) {
                $total+=$product->getPrice();
            }
        }
        // dd($commands);
        if (!$command) {
            throw $this->createNotFoundException("La commande n'existe pas");
        }
        else{
            return $this->render('command/show.html.twig', [
            'controller_name' => 'CommandController',
            'command' => $command, 
            'products'=>$products,
            'total'=>$total
        ]);
            }
    }



}

?>