<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    private $repository;

    public function __construct(ProductRepository $repository){
        $this->repository = $repository;
    }



    /**
     * @Route("/product", name="product")
     */
    public function ListProduct()
    {
        //dd($articles);
        $products = $this->repository->findAll();
        return $this->render('product/Products.html.twig', [
            'controller_name' => 'Product',
            'products' => $products,
        ]);
        
    }

    /**
     * @Route("/product/{id}", name="product.show")
     */
    public function show($id)
    {
        $productRepository = $this->getDoctrine()->getRepository(product::class);
        $product = $this->repository->find($id);
        if(!$product){
            throw $this->createNotFoundException('The Product Does Not Exist');
        }else{
            return $this->render('product/show.html.twig', [
                'controller_name' => 'ProductController',
                'product' => $product,
            ]);
        }

    }

    /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add(int $id, SessionInterface $session,ProductRepository $repository)
    {
        $product = $repository->find($id);

        if($product == NULL){

            return $this->json('nok', 404);

        }else{

            $panier = $session->get('panier', []);

            $panier[$id] = 1;

            $session->set('panier', $panier);

            return $this->json('ok', 200);

            $this->addFlash('success', "Le Produit {$product->getName()} a bien été ajouté au panier !");


        } 
    }

}

?>