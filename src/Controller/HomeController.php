<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    private $repository;

    public function __construct(ProductRepository $repository){
        $this->repository = $repository;
    }


    /**
     * @Route("/", name="index")
     */
    public function index(ProductRepository $repository)
    {
        //dd($articles);
        $mostproduct = $repository->findMostProductRecent(5);
        $minprice = $repository->findMinPriceProduct(5);
        return $this->render('index.html.twig', [
            'controller_name' => 'HomeController',
            'mostproduct' => $mostproduct,
            'minprice' => $minprice
        ]);
        
    }

}

?>