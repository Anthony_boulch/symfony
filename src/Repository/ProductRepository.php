<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }


    public function findMostProductRecent($value)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.createdAt','DESC')
            ->setMaxResults($value)
            ->getQuery()
            ->getResult()
        ;
    }


    
    public function findMinPriceProduct($value)
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.price','ASC')
            ->setMaxResults($value)
            ->getQuery()
            ->getResult()
        ;
    }
    
}
